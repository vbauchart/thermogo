FROM golang

COPY . /thermogo

WORKDIR /thermogo

RUN go build -o /usr/bin/thermogo cmd/thermogo/main.go

ENV ELASTICSEARCH_URL http://elasticsearch:9200

CMD /usr/bin/thermogo