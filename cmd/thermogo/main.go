package main

import (
	"flag"
	"log"
	"time"

	"gitlab.com/vbauchart/thermogo/internal/metrics"

	"gitlab.com/vbauchart/thermogo/internal/sensor"
)

func mainLoop(gpio_id int) {

	for {

		temperature, humidity, err := sensor.GetMeteo(gpio_id)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Temperature = %.1f°C, Humidity = %.1f%% \n",
			temperature, humidity)
		metrics.SendElastic(metrics.Meteo{Humidity: humidity, Temperature: temperature})

		time.Sleep(30 * time.Minute)
	}

}

func main() {
	gpioIdRef := flag.Int("n", -1, "GPIO ID")

	flag.Parse()

	log.Printf("Using GPIO ID %v", *gpioIdRef)

	mainLoop(*gpioIdRef)

}
