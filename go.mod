module gitlab.com/vbauchart/thermogo

go 1.16

require (
	github.com/d2r2/go-dht v0.0.0-20200119175940-4ba96621a218
	github.com/d2r2/go-logger v0.0.0-20181221090742-9998a510495e // indirect
	github.com/d2r2/go-shell v0.0.0-20191113051817-7664ea33645f // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20210506154932-f741c073f324
)
