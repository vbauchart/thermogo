package metrics

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
)

type Meteo struct {
	Temperature float64 `json:"temperature"`
	Humidity    float64 `json:"humidity"`
}

func SendElastic(meteo Meteo) {

	// log.SetFlags(0)

	body := fmt.Sprintf(`{"temperature": "%f","humidity": "%f", "@timestamp": "%s"}`, meteo.Temperature, meteo.Humidity, time.Now().Format("2006/01/02 15:04:05"))

	// PUT /meteo-2021
	// {
	// 	"mappings": {
	// 		"properties": {
	// 		  "@timestamp":    { "type": "date", "format": "yyyy/MM/dd HH:mm:ss" },
	// 		  "temperature":   { "type": "float","index": false }
	// 		  "humidity":      { "type": "float","index": false }
	// 	  }
	// 	}
	//   }

	var (
		r map[string]interface{}
		// wg sync.WaitGroup
	)

	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	res, err := es.Info()
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	// Check response status
	if res.IsError() {
		log.Fatalf("Error: %s", res.String())
	}

	// Deserialize the response into a map.
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print client and server version numbers.
	log.Printf("Client: %s", elasticsearch.Version)
	log.Printf("Server: %s", r["version"].(map[string]interface{})["number"])
	log.Println(strings.Repeat("~", 37))

	req := esapi.IndexRequest{
		Index:   "meteo-2021",
		Body:    strings.NewReader(body),
		Refresh: "true",
	}

	res, err = req.Do(context.Background(), es)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		log.Printf("[%s] Error indexing document ID=%d", res.Status(), 1)
	} else {
		// Deserialize the response into a map.
		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and indexed document version.
			log.Printf("[%s] %s; version=%d", res.Status(), r["result"], int(r["_version"].(float64)))
		}
	}

}
