package sensor

import (
	"log"

	"github.com/d2r2/go-dht"
)

func GetMeteo(gpio_id int) (float64, float64, error) {

	if gpio_id == -1 {
		log.Println("FAKE SENSOR")
		return 20.0, 50.0, nil
	}

	temperature, humidity, _, err := dht.ReadDHTxxWithRetry(dht.DHT22, gpio_id, false, 5)

	return float64(temperature), float64(humidity), err
}
