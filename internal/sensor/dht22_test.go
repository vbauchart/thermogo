package sensor

import "testing"

func TestGetMeteo(t *testing.T) {
	type args struct {
		gpio_id int
	}
	tests := []struct {
		name        string
		args        args
		temperature float64
		humidity    float64
		wantErr     bool
	}{
		{
			name:        "fake sensor",
			args:        args{-1},
			temperature: 20,
			humidity:    50,
			wantErr:     false,
		},
		{
			name:        "",
			args:        args{1000},
			temperature: -1,
			humidity:    -1,
			wantErr:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			temperature, humidity, err := GetMeteo(tt.args.gpio_id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMeteo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if temperature != tt.temperature {
				t.Errorf("GetMeteo() temperature = %v, want %v", temperature, tt.temperature)
			}
			if humidity != tt.humidity {
				t.Errorf("GetMeteo() humidity = %v, want %v", humidity, tt.humidity)
			}
		})
	}
}
